const express = require("express");
//mongoose is a package that allows creation of schemas to model our data structures and to have access to a number of methods for manipulating our database
const mongoose = require("mongoose");

const app = express();
const port = 3001;

//mongo db connection - needs to get connection string from mongo db atlas
//connecting to mongo db atlas
mongoose.connect(
  "mongodb+srv://vvaldez626:admin@wdc028-course-booking.0r6u7.mongodb.net/batch144-to-do?retryWrites=true&w=majority", //renaming the link to create a new collection instead of doing it in mongodb
  {
    //use both to prevent depracation warnings
    useNewUrlParser: true, //avoids current or future error while connecting same with topology
    useUnifiedTopology: true,
  }
);

// ERROR handling for mongo db and server
// set notification for connection success or failure
let db = mongoose.connection; //method from mongoose
// if error occurred, output in the console
db.on("error", console.error.bind(console, "connection error")); //on and once is like event listener; passes through these functions once it receives data; indicates what kind of error is present in the code
//if connection is successful output in the console
db.once("open", () => console.log("We're connected to the cloud database"));

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// = end of basic server =

//===== mongoose schemas ======

//schemas are structures of the docs we will use
//when clients give data these will be the templates
//acts as a blueprint for data
//will use schema() constructor of the mongoose module to create a new schema object
//every schemas has a model
//schemas are the blueprint for the data
const taskSchema = new mongoose.Schema({
  name: String,
  status: {
    type: String,
    //default values are the predefined values for a field if we don't put any value
    default: "pending",
  },
});

//model allows us to gain access to schema and can perform crud methods - variable should be capitalized to follow MVC approach for naming convention (model view controller) - single and capitalized; every schema will end up in a model
const Task = mongoose.model("Task", taskSchema); //(name/where the data will be collected, variable name of schema)
//models are what allows us to gain access to methods that will perform crud functions; model acts as middleman for server and database
//models must be in singular form and capitalized following the MVC approach for naming conventions
//firstparameter of the mongoose model method indicates the collection in where to store the data
//second parameter is used to specify the schema/blueprint

//mongo db saves the name of the model and uses it to name the collection but will add an s and will not capitalize - Task will become tasks

//ROUTES

//create a new task
/* 
  Business logic 
  1. Add a functionality to check if there are duplicates tasks 
    - if the task already exists, return there is a duplicate
    - if the task doesn't exist, can add to database
  2. The task data will be coming from the request's body 
  3. Create new Task object with properties that we need
  4. Save the data  
*/

app.post("/tasks", (req, res) => {
  //Check if there are duplicate tasks
  //findOne() is a mongoose method that acts similar to "find", returns the first document that matches the search criteria
  Task.findOne({ name: req.body.name }, (error, result) => {
    //first critera - object name and value
    //error parameter comes first before successful or result
    // if a document was found and the document's name matches the info sent via client/postman
    if (result !== null && result.name == req.body.name) {
      //return a message to client/postman
      return res.send("Duplicate task found");
    } else {
      //if no document was found
      //create a new task and save it to the database
      let newTask = new Task({
        name: req.body.name,
      });

      newTask.save((saveErr, savedTask) => {
        //if there are errors in saving
        if (saveErr) {
          return console.error(saveErr); //saveErr will print any errors found in the console, saveErr is an error object that will contain details about the error
          //error normally comes as an object data type
        } else {
          //no error found while creating the document
          //return a status code of 201 for successful creation
          return res.status(201).send("New Task Created");
        }
      }); //saves task to the database; error comes before saved task
    }
  });
});

/* 
  Business logic for RETRIEVING ALL DATA 
  1. retrieve all the documents using the find()
  2. print errors if encountered
  3. if no errors are found, send a success status back to the client and return an array of documents 
*/

app.get("/tasks", (req, res) => {
  // set empty object to return all the documents and stores them in the 'result' parameter of the callback function
  Task.find({}, (err, result) => {
    //if an error occurred
    if (err) {
      return console.log(err);
    } else {
      return res.status(200).json({
        data: result,
      }); //.json methods sends a json format in the response
    }
  });
});

/* 
  Business logic for Register a User
  1. Find if there are duplicate users 
    - if user already exists, we return an error 
    - if user doesn't exist, we add it in the database 
    - if the username and password are both not blank 
      - if blank, send response "both username and password must be provided"
      - create a new object if both conditions have been met 
      - save the object 
        - if error return an error message 
        - else, response a status 201 for creation and new user registered 

*/

//  ==== ACTIVITY ====

// -- user schema & model --
const userSchema = new mongoose.Schema({
  username: String,
  password: String,
});

const User = mongoose.model("User", userSchema);

app.post("/signup", (req, res) => {
  User.findOne(
    { username: req.body.username, password: req.body.password },
    (error, result) => {
      if (result !== null && result.username == req.body.username) {
        return res.send("Duplicate task found");
      } else if (req.body.username == null || req.body.password == null) {
        return res.send("Please provide BOTH username and password");
      } else {
        let newUser = new User({
          username: req.body.username,
          password: req.body.password,
        });

        newUser.save((saveErr, savedUser) => {
          if (saveErr) {
            return console.log(saveErr);
          } else {
            return res.status(201).send("New user created");
          }
        });
      }
    }
  );
});

app.get("/users", (req, res) => {
  User.find({}, (err, result) => {
    if (err) {
      return console.log(err);
    } else {
      return res.status(200).json({
        data: result,
      });
    }
  });
});

app.listen(port, () => {
  `Server is running at port ${port}`;
});

// npx kill-port portNumber - kills ports; use rarely
